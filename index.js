
function iniciaModal (modalID){
    const modal = document.getElementById(modalID);
    if (modal) {
    modal.classList.add('modal-open');
    modal.addEventListener('click', (e) => {
        if(e.target.id == modalID || e.target.className == 'button-close page_close') {
            modal.classList.remove('modal-open');
        }
    });
  }
}


const button = document.querySelector('.bag-button')
button.addEventListener('click' , () => iniciaModal('modal-form'));